package com.cmind.testapp.list;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ListItemGenerator {

    private static final Random RND = new Random();
    private static final CharSequence CHAR_POOL = "0123456789";

    private final StringBuilder stringBuilder = new StringBuilder();

    public List<String> generate(int count) {
        final List<String> list = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            list.add(generateRandomItem());
        }
        return list;
    }

    private String generateRandomItem() {
        return generateRandomNumberString();
    }

    private String generateRandomNumberString() {
        char lastChar = getRandomChar();
        int lastCount = 1;

        stringBuilder.setLength(0);
        final int rndLength = RND.nextInt(19);
        if (rndLength == 0) {
            append(lastChar, lastCount);
        } else {
            for (int i = 0; i < rndLength; i++) {
                final char rndChar = getRandomChar();
                if (rndChar == lastChar) {
                    lastCount++;

                    if (i == rndLength - 1) {
                        append(lastChar, lastCount);
                    }
                } else {
                    append(lastChar, lastCount);

                    lastChar = rndChar;
                    lastCount = 1;
                }
            }
        }
        stringBuilder.setLength(stringBuilder.length() - 2);
        return stringBuilder.toString();
    }

    private void append(char c, int count) {
        stringBuilder.append(getNumberString(count));
        stringBuilder.append(" ");
        stringBuilder.append(c);
        stringBuilder.append(", ");
    }

    private char getRandomChar() {
        return CHAR_POOL.charAt(RND.nextInt(9));
    }

    private String getNumberString(int number) {
        switch (number) {
            case 0: return "Zero";
            case 1: return "One";
            case 2: return "Two";
            case 3: return "Three";
            case 4: return "Four";
            case 5: return "Five";
            case 6: return "Six";
            case 7: return "Seven";
            case 8: return "Eight";
            case 9: return "Nine";
        }

        throw new IllegalArgumentException("Number must be within interval 0..9, got "
                + String.valueOf(number));
    }
}
