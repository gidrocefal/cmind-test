package com.cmind.testapp.list;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cmind.testapp.MainActivity;
import com.cmind.testapp.R;

import java.util.List;

public class ListFragment extends Fragment {

    public interface ListFragmentListener {
        void onFirstItemClick();
    }

    public static ListFragment newInstance() {
        return new ListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final ListItemGenerator generator = new ListItemGenerator();
        final ListAdapter adapter = new ListAdapter(
                getContext(),
                generator.generate(100),
                (MainActivity) getActivity()
        );

        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.items_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    private static class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

        private final LayoutInflater mInflater;
        private final List<String> mItems;
        private final ListFragmentListener mListener;

        public ListAdapter(Context context, List<String> items, ListFragmentListener listener) {
            mInflater = LayoutInflater.from(context);
            mItems = items;
            mListener = listener;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            final View view = mInflater.inflate(R.layout.item_list, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            final String title = mItems.get(position);
            holder.setTitle(title);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.getAdapterPosition() == 0) {
                        mListener.onFirstItemClick();
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        static class ViewHolder extends RecyclerView.ViewHolder {

            private TextView mTitleView;

            public ViewHolder(View itemView) {
                super(itemView);
                mTitleView = (TextView) itemView.findViewById(R.id.list_item_title);
            }

            public void setTitle(String title) {
                mTitleView.setText(title);
            }

        }
    }
}
