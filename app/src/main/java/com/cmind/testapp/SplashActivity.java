package com.cmind.testapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;

public class SplashActivity extends AppCompatActivity {

    private static final int DELAY_IN_SEC = 3;

    private Subscription mSubscription;

    @Override
    protected void onResume() {
        super.onResume();
        mSubscription = getDelayedObservable(DELAY_IN_SEC)
                .subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object o) {
                        showMainScreen();
                    }
                });
    }

    @Override
    protected void onPause() {
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
        super.onPause();
    }

    private Observable<Object> getDelayedObservable(int delayInSec) {
        return Observable.just(null).delay(delayInSec, TimeUnit.SECONDS);
    }

    private void showMainScreen() {
        final Intent mainScreenIntent = MainActivity.buildIntent(this);
        startActivity(mainScreenIntent);
        finish();
    }
}
