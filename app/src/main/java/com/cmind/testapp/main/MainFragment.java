package com.cmind.testapp.main;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.cmind.testapp.BackPressHandler;
import com.cmind.testapp.R;
import com.squareup.picasso.Picasso;

public class MainFragment extends Fragment implements BackPressHandler {

    public interface MainFragmentListener {
        void onYahooUrlClick();
    }

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    private static final String[] IMAGES = {
            "http://3zvzd.blob.core.windows.net/mimimi/26901.jpg",
            "http://3zvzd.blob.core.windows.net/mimimi/26890.jpg",
            "http://3zvzd.blob.core.windows.net/mimimi/26876.jpg",
            "http://3zvzd.blob.core.windows.net/mimimi/26864.jpg",
            "http://3zvzd.blob.core.windows.net/mimimi/26858.jpg",
            "http://3zvzd.blob.core.windows.net/mimimi/26852.jpg",
            "http://3zvzd.blob.core.windows.net/mimimi/26842.jpg",
            "http://3zvzd.blob.core.windows.net/mimimi/26838.jpg"
    };

    private static final String STATE_IMAGE_INDEX = "state.image_index";

    private WebViewWidget mWebViewWidget;

    private PictureWidget mPictureWidget;
    private ImageLoader mImageLoader;
    private int mCurrentImageIndex = -1;

    private MainFragmentListener mListener;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_IMAGE_INDEX, mCurrentImageIndex);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mListener = (MainFragmentListener) getActivity();

        mWebViewWidget = new WebViewWidget((ViewGroup) view.findViewById(R.id.main_web_view_widget));
        mWebViewWidget.setListener(new WebViewWidget.WebViewWidgetListener() {
            @Override
            public void onYahooClick() {
                mListener.onYahooUrlClick();
            }
        });

        mPictureWidget = new PictureWidget((ViewGroup) view.findViewById(R.id.main_picture_widget));
        mPictureWidget.setViewModel(new PictureWidget.ViewModel.Message("Tap to load picture."));
        mPictureWidget.setListener(new PictureWidget.Listener() {
            @Override
            public void onClick() {
                loadNextImage();
            }
        });

        mImageLoader = new ImageLoader(Picasso.with(getActivity()));
        mImageLoader.setListener(new ImageLoader.Listener() {
            @Override
            public void onStartLoading() {
                mPictureWidget.setViewModel(new PictureWidget.ViewModel.Loading());
            }

            @Override
            public void onLoaded(Bitmap bitmap) {
                mPictureWidget.setViewModel(new PictureWidget.ViewModel.Loaded(bitmap));
            }

            @Override
            public void onError() {
                mPictureWidget.setViewModel(new PictureWidget.ViewModel.Message(
                        "Can't load picture. Tap to retry."));
            }
        });

        if (savedInstanceState != null) {
            mCurrentImageIndex = savedInstanceState.getInt(STATE_IMAGE_INDEX, -1);
        }
    }

    @Override
    public boolean onBackPressed() {
        final WebView webView = mWebViewWidget.getWebView();
        if (webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mCurrentImageIndex > -1) {
            loadImageAtIndex(mCurrentImageIndex);
        }

        mWebViewWidget.loadGoogle();
    }

    private void loadNextImage() {
        mCurrentImageIndex++;

        if (mCurrentImageIndex > IMAGES.length - 1) {
            mCurrentImageIndex = 0;
        }

        loadImageAtIndex(mCurrentImageIndex);
    }

    private void loadImageAtIndex(int index) {
        if (index < 0 || index > IMAGES.length - 1) {
            throw new IllegalArgumentException(
                    "Index out of bounds " + String.valueOf(index) + " with size: " + String.valueOf(IMAGES.length));
        }

        final String imageUrl = IMAGES[index];
        mImageLoader.loadImage(imageUrl);
    }
}
