package com.cmind.testapp.main;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.cmind.testapp.R;

public class WebViewWidget {

    public interface WebViewWidgetListener {
        void onYahooClick();
    }

    private final WebView mWebView;

    private final View mErrorBlock;
    private final View mErrorGoogleButton;
    private final View mErrorRetryButton;

    private WebViewWidgetListener mListener;

    public WebViewWidget(ViewGroup root) {
        mWebView = (WebView) root.findViewById(R.id.main_web_view);
        setupWebView(mWebView);

        mErrorBlock = root.findViewById(R.id.main_web_view_error);
        mErrorGoogleButton = root.findViewById(R.id.main_web_view_error_go_google);
        mErrorRetryButton = root.findViewById(R.id.main_web_view_error_retry);

        mWebView.setVisibility(View.VISIBLE);
        mErrorBlock.setVisibility(View.GONE);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setupWebView(WebView webView) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        webView.getSettings().setSupportMultipleWindows(false);
        webView.getSettings().setSupportZoom(false);
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setWebViewClient(new WebViewClient() {

            private boolean mFinishedWithError = false;

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mFinishedWithError = false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (mFinishedWithError) {
                    mErrorBlock.setVisibility(View.VISIBLE);
                } else {
                    mErrorBlock.setVisibility(View.GONE);
                }
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                onReceivedErrorInternal(view, Uri.parse(failingUrl));
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                onReceivedErrorInternal(view, request.getUrl());
            }

            private void onReceivedErrorInternal(WebView view, final Uri url) {
                mFinishedWithError = true;

                mErrorBlock.setVisibility(View.VISIBLE);
                mErrorRetryButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mWebView.loadUrl(url.toString());
                    }
                });
                mErrorGoogleButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        loadGoogle();
                    }
                });
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String stringUrl) {
                final Uri url = Uri.parse(stringUrl);
                return shouldOverrideUrlLoadingInternal(view, url);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                final Uri url = request.getUrl();
                return shouldOverrideUrlLoadingInternal(view, url);
            }

            private boolean shouldOverrideUrlLoadingInternal(WebView view, Uri url) {
                if (isYahooHost(url)) {
                    if (mListener != null) {
                        mListener.onYahooClick();
                    }

                    return true;
                }

                return false;
            }
        });
    }

    private boolean isYahooHost(Uri url) {
        final String host = getTopLevelHost(url);
        return host != null && host.equals("yahoo.com");
    }

    @Nullable
    private String getTopLevelHost(Uri url) {
        final String host = url.getHost();
        final String[] segments = host.split("\\.");
        if (segments.length > 1) {
            return String.format("%s.%s", segments[segments.length - 2], segments[segments.length - 1]);
        } else {
            return null;
        }
    }

    public void setListener(WebViewWidgetListener listener) {
        mListener = listener;
    }

    public WebView getWebView() {
        return mWebView;
    }

    public void loadGoogle() {
        mWebView.loadUrl("https://www.google.com");
    }
}
