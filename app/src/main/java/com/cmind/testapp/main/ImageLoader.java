package com.cmind.testapp.main;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class ImageLoader {

    public interface Listener {
        void onStartLoading();
        void onLoaded(Bitmap bitmap);
        void onError();
    }

    private Listener mListener;
    private final Picasso mPicasso;
    private final Target mTarget = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            if (mListener != null) {
                mListener.onLoaded(bitmap);
            }
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            if (mListener != null) {
                mListener.onError();
            }
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {}
    };

    public ImageLoader(Picasso picasso) {
        mPicasso = picasso;
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    public void loadImage(String imageUrl) {
        mPicasso.cancelRequest(mTarget);

        if (mListener != null) {
            mListener.onStartLoading();
        }
        mPicasso.load(imageUrl).into(mTarget);
    }
}
