package com.cmind.testapp.main;

import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cmind.testapp.R;

public class PictureWidget {

    public interface ViewModel {
        class Loading implements ViewModel {}

        class Loaded implements ViewModel {
            private final Bitmap bitmap;
            public Loaded(Bitmap bitmap) {
                this.bitmap = bitmap;
            }
        }

        class Message implements ViewModel {
            private final String message;
            public Message(String message) {
                this.message = message;
            }
        }
    }

    public interface Listener {
        void onClick();
    }

    private Listener mListener;
    private boolean mClickEnabled;

    private final ImageView mImageView;
    private final View mProgressView;
    private final TextView mMessageView;

    public PictureWidget(ViewGroup root) {
        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null && mClickEnabled) {
                    mListener.onClick();
                }
            }
        });

        mImageView = (ImageView) root.findViewById(R.id.main_picture_widget_image);
        mProgressView = root.findViewById(R.id.main_picture_widget_progress);
        mMessageView = (TextView) root.findViewById(R.id.main_picture_widget_message);
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    public void setViewModel(ViewModel viewModel) {
        if (viewModel instanceof ViewModel.Loading) {
            mClickEnabled = false;
            mImageView.setVisibility(View.GONE);
            mProgressView.setVisibility(View.VISIBLE);
            mMessageView.setVisibility(View.GONE);
        } else if (viewModel instanceof ViewModel.Loaded) {
            final ViewModel.Loaded loadedViewModel = (ViewModel.Loaded) viewModel;
            mClickEnabled = true;
            mImageView.setVisibility(View.VISIBLE);
            mImageView.setImageBitmap(loadedViewModel.bitmap);
            mProgressView.setVisibility(View.GONE);
            mMessageView.setVisibility(View.GONE);
        } else if (viewModel instanceof ViewModel.Message) {
            final ViewModel.Message messageViewModel = (ViewModel.Message) viewModel;
            mClickEnabled = true;
            mImageView.setVisibility(View.GONE);
            mProgressView.setVisibility(View.GONE);
            mMessageView.setVisibility(View.VISIBLE);
            mMessageView.setText(messageViewModel.message);
        } else {
            throw new IllegalStateException("Unknown ViewModel type");
        }
    }
}
