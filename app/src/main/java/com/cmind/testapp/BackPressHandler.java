package com.cmind.testapp;

public interface BackPressHandler {
    boolean onBackPressed();
}
