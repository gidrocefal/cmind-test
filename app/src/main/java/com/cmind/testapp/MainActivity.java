package com.cmind.testapp;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.cmind.testapp.list.ListFragment;
import com.cmind.testapp.main.MainFragment;

public class MainActivity extends AppCompatActivity implements
        MainFragment.MainFragmentListener,
        ListFragment.ListFragmentListener
{
    public static Intent buildIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    private static final String STATE_FRAGMENT_MAIN = "state.fragment_main";
    private static final String STATE_FRAGMENT_LIST = "state.fragment_list";

    private final FragmentManager mFragmentManager = getSupportFragmentManager();

    private Fragment mMainFragment;
    private Fragment mListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            mMainFragment = mFragmentManager.getFragment(savedInstanceState, STATE_FRAGMENT_MAIN);
            mListFragment = mFragmentManager.getFragment(savedInstanceState, STATE_FRAGMENT_LIST);
        } else {
            showMainScreen();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (mMainFragment != null) {
            mFragmentManager.putFragment(outState, STATE_FRAGMENT_MAIN, mMainFragment);
        }

        if (mListFragment != null) {
            mFragmentManager.putFragment(outState, STATE_FRAGMENT_LIST, mListFragment);
        }
    }

    @Override
    public void onBackPressed() {
        final FragmentManager fm = getSupportFragmentManager();
        final Fragment fragment = fm.findFragmentById(R.id.content_container);
        if (fragment instanceof BackPressHandler) {
            final BackPressHandler backPressHandler = (BackPressHandler) fragment;
            if (backPressHandler.onBackPressed()) {
                return;
            }
        }

        super.onBackPressed();
    }

    private void showMainScreen() {
        showFragmentWithBackstack(MainFragment.newInstance(), "main_fragment");
    }

    private void showListScreen() {
        showFragmentWithBackstack(ListFragment.newInstance(), "list_fragment");
    }

    private void showFragmentWithBackstack(Fragment fragment, String backstackName) {
        mFragmentManager.beginTransaction()
                .replace(R.id.content_container, fragment)
                .addToBackStack(backstackName)
                .commit();
    }

    @Override
    public void onYahooUrlClick() {
        showListScreen();
    }

    @Override
    public void onFirstItemClick() {
        mFragmentManager.popBackStack("main_fragment", 0);
    }
}
